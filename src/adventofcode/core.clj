(ns adventofcode.core)
;day 1

(def int-first-star (map (fn [^Character c] (Character/digit c 10)) (slurp "src/adventofcode/day1.in")))

(defn add-matching [s]
  (defn add-recur [s-recur accum]
    (cond (empty? s-recur) accum
          (empty? (rest s-recur)) (if (= (first s-recur) (first s))
                                    (+ accum (first s-recur))
                                    accum)
          (= (first s-recur) (first (rest s-recur))) (add-recur (rest s-recur) (+ accum (first s-recur)))
          :else (add-recur (rest s-recur) accum)))
  (add-recur s 0))

; day 2
(def table-data (list (list 116	1470	2610	179	2161	2690	831	1824	2361	1050	2201	118	145	2275	2625	2333)
                      (list 976	220	1129	553	422	950	332	204	1247	1092	1091	159	174	182	984	713)
                      (list 84	78	773	62	808	83	1125	1110	1184	145	1277	982	338	1182	75	679)
                      (list 3413	3809	3525	2176	141	1045	2342	2183	157	3960	3084	2643	119	108	3366	2131)
                      (list 1312	205	343	616	300	1098	870	1008	1140	1178	90	146	980	202	190	774)
                      (list 4368	3905	3175	4532	3806	1579	4080	259	2542	221	4395	4464	208	3734	234	4225)
                      (list 741	993	1184	285	1062	372	111	118	63	843	325	132	854	105	956	961)
                      (list 85	79	84	2483	858	2209	2268	90	2233	1230	2533	322	338	68	2085	1267)
                      (list 2688	2022	112	130	1185	103	1847	3059	911	107	2066	1788	2687	2633	415	1353)
                      (list 76	169	141	58	161	66	65	225	60	152	62	64	156	199	80	56)
                      (list 220	884	1890	597	3312	593	4259	222	113	2244	3798	4757	216	1127	4400	178)
                      (list 653	369	216	132	276	102	265	889	987	236	239	807	1076	932	84	864)
                      (list 799	739	75	1537	82	228	69	1397	1396	1203	1587	63	313	1718	1375	469)
                      (list 1176	112	1407	136	1482	1534	1384	1202	604	851	190	284	1226	113	114	687)
                      (list 73	1620	81	1137	812	75	1326	1355	1545	1666	1356	1681	1732	85	128	902)
                      (list 571	547	160	237	256	30	496	592	385	576	183	692	192	387	647	233)))

(defn create-min-max-list [table-data] (map list (map #(reduce max %) table-data) (map #(reduce min %) table-data)))

(defn create-diff-list [min-max-list]
  (map #(reduce - %) min-max-list))

(defn checksum [table-data]
  (reduce + (create-diff-list (create-min-max-list table-data))))

;day 3

(def dir-offset {:SE 0 :S -1 :SW -2 :W -3 :NW -4 :N -5 :NE -6 :E -7})

(defn get-value-for-spiral [spiral direction]
  "This gets the value along a direction a distance spiral from the origin"
  (let [offset (direction dir-offset)]
    (loop [i spiral
           sum 0]
      (if (= i 0)
        (inc sum)
        (recur (dec i) (+ sum (+ (* i 8) offset)))))))

(defn in-spiral-range? [value spiral]
  "This decides whether a value is in a given distance from the origin"
  (and (<= value (get-value-for-spiral spiral :SE)) (> value (get-value-for-spiral (dec spiral) :SE))))

(defn get-max-for-spiral [spiral]
  "This gives the max value for a distance spiral from the origin"
  (get-value-for-spiral spiral :SE))

(defn get-min-for-spiral [spiral]
  "The opposite of a max :)"
  (inc (- (get-max-for-spiral spiral) (* 8 spiral))))

(defn in-north-bound-range? [value spiral]
  "This defines whether the value is in the column to the right of the origin for a given distance from the origin"
  (and (<= value (get-value-for-spiral spiral :NE)) (>= value (get-min-for-spiral spiral))))

(defn in-west-bound-range? [value spiral]
  "This defines whether the value is in the row to the top of the origin for a given distance from the origin"
  (and (<= value (get-value-for-spiral spiral :NW)) (>= value (get-value-for-spiral spiral :NE))))

(defn in-south-bound-range? [value spiral]
  "This defines whether the value is in the column to the left of the origin for a given distance from the origin"
  (and (<= value (get-value-for-spiral spiral :SW)) (>= value (get-value-for-spiral spiral :NW))))

(defn in-east-bound-range? [value spiral]
  "This defines whether the value is in the row to the bottom of the origin for a given distance from the origin"
  (and (<= value (get-value-for-spiral spiral :SE)) (>= value (get-value-for-spiral spiral :SW))))

(defn find-spiral-for-value [value]
  "This finds the distance from the origin which contains the value"
  (loop [spiral 1]
    (if (and (<= value (get-max-for-spiral spiral)) (>= value (get-min-for-spiral spiral)))
      spiral
      (recur (inc spiral)))))

(defn find-row-for-value [value]
  "This finds the column/row which contains the value"
  (let [spiral (find-spiral-for-value value)]
    (cond (in-north-bound-range? value spiral) (list spiral :NORTH)
          (in-west-bound-range? value spiral) (list spiral :WEST)
          (in-south-bound-range? value spiral) (list spiral :SOUTH)
          (in-east-bound-range? value spiral) (list spiral :EAST))))

(defn find-vector-for-value [value]
  "This creates a vector (x, y) which points to the value"
  (let [row (find-row-for-value value)
        direction (first (rest row))
        spiral (first row)]
    (cond (= direction :WEST)
          (let [x-offset (- (get-value-for-spiral spiral :NE) value)]
            (list (+ spiral x-offset) spiral))
          (= direction :EAST)
          (let [x-offset (- value (get-value-for-spiral spiral :SE))]
            (list (+ spiral x-offset) (- spiral)))
          (= direction :SOUTH)
          (let [y-offset (- (get-value-for-spiral spiral :NW) value)]
            (list (- spiral) (+ spiral y-offset)))
          (= direction :NORTH)
          (let [y-offset (- value (get-value-for-spiral spiral :NE))]
            (list spiral (+ spiral y-offset))))))

(defn abs [n]
  "The absolute value"
  (if (< n 0) (- n)
      n))

(defn vector-size [vector]
  "This gets the manhattan steps thing"
  (let [x (first vector)
        y (first (rest vector))]
    (+ (abs x) (abs y))))

; Keegans outlier challenge

(defn has-single-outlier? [outlier-fn input]
  (let [num-outliers (reduce + (map #(if (outlier-fn %) 1 0) input))]
    (= 1 num-outliers)))

(defn get-first-outlier [outlier-fn input]
  (loop [value 0
         input input]
    (if (empty? input)
      value
      (recur (if (outlier-fn (first input)) (first input) value) (rest input)))))

(defn find-outlier [outlier-fn input]
  (let [has-outlier (has-single-outlier? outlier-fn input)]
    (if has-outlier (get-first-outlier outlier-fn input) 0)))

(defn contains-odd? [data]
  (cond (empty? data) false
        (odd? (first data)) true
        :else (contains-odd? (rest data))))

(defn get-odd-from-data [data]
  (loop [inputs data]
     (cond (empty? inputs) 0
           (odd? (first inputs))
           (first inputs)
           :else
       (recur (rest inputs)))))

(defn find-odd-outlier [data]
  (loop [data data]
    (cond (empty? data) 0
         (= 0 (get-odd-from-data data)) 0
         (when (contains-odd? data)
           (if (= 0 (get-odd-from-data (rest data)))
             (get-odd-from-data data)
             0)) 0
         :else 0)))


; 4th star

(defn uniquer [phrase]
  (let [phrase (clojure.string/split phrase #" ")
       size-phrase (count phrase)
       phrase-set (into #{} phrase)
       size-unique (count phrase-set)]
   (if (not= size-phrase size-unique)
     1
     0)))

(defn num-dups []
  (let [pass-phrases (clojure.string/split-lines (slurp "src/adventofcode/day4.in"))]
    (- (count pass-phrases) (reduce + (map #(uniquer %) pass-phrases)))))

;day 5

(defn pos-map [vecs]
  (loop [key 0
         vals vecs
         maps {}]
    (if (empty? vals)
      maps
      (recur (inc key) (rest vals) (assoc maps key (first vals))))))

(defn get-and-inc [input-map key]
  (let [prev-val (get input-map key :tilt)]
    (if (= :tilt prev-val)
      (list input-map prev-val)
      (list (assoc input-map key (inc prev-val)) (+ key prev-val)))))

(defn get-map [map-val]
  (first map-val))

(defn get-val [map-val]
  (first (rest map-val)))

(defn jump-to-escape [input-map]
  (loop [input-map input-map
         accum 0
         map-val (get-and-inc input-map 0)]
    (if (= :tilt (get-val map-val))
      accum
      (recur (get-map map-val) (inc accum) (get-and-inc (get-map map-val) (get-val map-val))))))


; Day 6
(def banks [11	11	13	7	0	15	5	5	4	4	1	1	7	1	15	11])

(defn get-max-entry [bank-pos-map]
  (let [max-val (reduce max (map val bank-pos-map))]
    (loop [bank-pos-map bank-pos-map]
     (let [max-entry (first bank-pos-map)]
       (if (or (= max-val (val max-entry)) (empty? bank-pos-map))
        max-entry
        (recur (rest bank-pos-map)))))))

(defn pos-distribute [banks max-index]
  (loop [max-val (get banks max-index)
         dist-vec (assoc banks max-index 0)
         next-pos (mod (inc max-index) (count banks))]
    (if (= 0 max-val)
      dist-vec
      (recur (dec max-val) (assoc dist-vec next-pos (inc (get dist-vec next-pos))) (mod (inc next-pos) (count dist-vec))))))

(defn get-max-pos [banks]
  (let [max-val (reduce max banks)]
    (.indexOf banks max-val)))

(defn pos-find-dist-cycle [banks]
  (loop [distribute-banks (pos-distribute banks (get-max-pos banks))
         dist-set (conj #{} banks distribute-banks)]
    (let [next-distribute-banks (pos-distribute distribute-banks (get-max-pos distribute-banks))
          next-dist-set (conj dist-set next-distribute-banks)]
      (if (= (count next-dist-set) (count dist-set))
        (count next-dist-set)
        (recur next-distribute-banks next-dist-set)))))

;day7

(defn is-leaf? [node]
  (not (.contains node "->")))

(defn get-trees [input] (filter #(.contains % "->") (clojure.string/split-lines input)))

(defn get-children [trees] (map (fn [v] (map #(clojure.string/trim %) v)) (map #(clojure.string/split (first %) #",") (map #(drop 1 %) (map #(clojure.string/split % #"->") trees)))))

(defn get-nodes [trees]
  (map #(first %) (map #(clojure.string/split % #"\s") (map #(clojure.string/trim %) trees))))

(defn find-parent [input]
  (let [trees (get-trees input)
        children-set (into #{} (flatten (get-children trees)))
        nodes (get-nodes trees)]
    (filter #(not (contains? children-set %)) nodes)))

(def day7 (slurp "src/adventofcode/day7.in"))

; day 8

(def day8 (clojure.string/split-lines (slurp "src/adventofcode/day8.in")))

(defn get-action [act-con]
  (first act-con))

(defn get-condition [act-con]
  (first (rest act-con)))

(defn get-register [action]
  (first (clojure.string/split action #" ")))

(defn get-command-fn [action]
  (let [split-action (clojure.string/split action #" ")
        command (first (rest split-action))
        arg (Integer. (first (rest (rest split-action))))]
    (cond (= "inc" command) (partial + arg)
          (= "dec" command) (fn [other] (- other arg)))))

(defn get-comparison-fn [command]
  (if (> (count command) 1)
    (let [first-char (subs command 0 1)
         rest-chars (subs command 1)]
     (if (= "!" first-char)
       (comp not (resolve (symbol rest-chars)))
       (resolve (symbol command))))
    (resolve (symbol command))))

(defn get-condition-fn [condition]
  (let [split-cond (clojure.string/split condition #" ")
        comparison-fn (get-comparison-fn (first (rest split-cond)))
        cond-arg (Integer. (first (rest (rest split-cond))))]
    (fn [other] (comparison-fn other cond-arg))))

(defn get-condition-reg [condition]
  (let [split-cond (clojure.string/split condition #" ")]
    (first split-cond)))

(defn run-through-actions [act-cons]
  (loop [actions (map #(get-action %) act-cons)
         conditions (map #(get-condition %) act-cons)
         accum-map {}]
    (if (empty? actions)
      accum-map
      (recur (rest actions) (rest conditions)
             (let [reg (get-register (first actions))
                   current-val (get accum-map reg 0)
                   condition-fn (get-condition-fn (first conditions))
                   command-fn (get-command-fn (first actions))
                   condition-val (get accum-map (get-condition-reg (first conditions)) 0)]
               (if (condition-fn condition-val)
                 (assoc accum-map reg (command-fn current-val))
                 accum-map))))))

(defn get-max-reg-val [register-map]
  (reduce max (map #(val %) register-map)))

(defn parse-action-conditions [lines]
  (map (fn [v] (map #(clojure.string/trim %) v)) (map #(clojure.string/split % #"if") lines)))

;day 10
(defn get-wrapped-vec [start length input-vec]
  (let [dist (+ start length)]
    (if (<= dist (count input-vec))
      (subvec input-vec start dist)
      (let [wrapped-offset (- dist (count input-vec))]
        (vec (flatten (conj (subvec input-vec start) (subvec input-vec 0 wrapped-offset))))))))

(defn get-wrapped-index [index list-size]
  (mod index list-size))

(defn assoc-vals [hash-vec new-vec start-index]
  (loop [current-index start-index
         return-vec hash-vec
         new-vec new-vec]
    (if (empty? new-vec)
      return-vec
      (recur (get-wrapped-index (inc current-index) (count return-vec))
             (assoc return-vec current-index (first new-vec))
             (rest new-vec)))))

(defn knot-hash [lengths]
  (loop [current-pos 0
         skip-size 0
         hash-vec (vec (range 256))
         lengths lengths]
    (if (empty? lengths)
      hash-vec
      (let [current-length (first lengths)
            reversed-wrapped-vec
            (vec (reverse (get-wrapped-vec current-pos current-length hash-vec)))
            updated-hash-vec (assoc-vals hash-vec reversed-wrapped-vec current-pos)]
        (recur (get-wrapped-index (+ current-pos current-length skip-size) (count hash-vec)) (inc skip-size) updated-hash-vec (rest lengths))))))

(defn get-day10-input []
  (vec (map #(Integer. %) (clojure.string/split (clojure.string/trim-newline (slurp "src/adventofcode/day10.in")) #","))))

;day 11
(defn get-day11-input [string]
  (map #(keyword %) (clojure.string/split string #",")))

(defn vec-add [vec1 vec2]
  (list (+ (first vec1) (first vec2)) (+ (first (rest vec1)) (first (rest vec2)))))

(defn scalar-mult [vector scalar]
  (list (* scalar (first vector)) (* scalar (first (rest vector)))))

(defn populate-dir-to-vector-map []
  (let [dir-to-vector {:s (list 0 -1)
                    :e (list 1 0)
                    :n (list 0 1)
                    :w (list -1 0)}]
    (assoc dir-to-vector :se (vec-add (get dir-to-vector :s) (get dir-to-vector :e))
           :ne (vec-add (get dir-to-vector :n) (get dir-to-vector :e))
           :sw (vec-add (get dir-to-vector :s) (get dir-to-vector :w))
           :nw (vec-add (get dir-to-vector :n) (get dir-to-vector :w)))))

(def dir-to-vector {:n '(0 -1)
                    :nw '(-1 0)
                    :ne '(1 -1)
                    :sw '(-1 1)
                    :s '(0 1)
                    :se '(1 0)})

(defn run-through-directions [dirs]
  (reduce (fn [a b]
            (vec-add (if (seq? a)
                       a
                       (get dir-to-vector a))
                     (get dir-to-vector b (list 0 0)))) dirs))

(defn translate-vector [vector dir how-much]
  (vec-add vector (scalar-mult (get dir-to-vector dir) (- how-much))))

(defn get-min-steps-to-vector [vector]
  (let [x (first vector)
        y (first (rest vector))
        translate-n (min (abs x) (abs y))]
    (cond (and (pos? x) (pos? y)) (+ translate-n (get-min-steps-to-vector (translate-vector vector :ne translate-n))) 
          (and (neg? x) (pos? y)) (+ translate-n (get-min-steps-to-vector (translate-vector vector :nw translate-n)))
          (and (neg? x) (neg? y)) (+ translate-n (get-min-steps-to-vector (translate-vector vector :sw translate-n)))
          (and (pos? x) (neg? y)) (+ translate-n (get-min-steps-to-vector (translate-vector vector :se translate-n)))
          (= 0 x) (abs y)
          (= 0 y) (abs x))))

(defn euclid-vector-size [vector]
  (let [x (first vector)
        y (first (rest vector))]
    (Math/sqrt (+ (* x x) (* y y)))))

(defn hex-dist [hex-vec]
  (let [x (first hex-vec)
        y (first (rest hex-vec))]
    (/ (+ (abs x) (abs y) (+ x y)) 2)))

(defn accum-steps [dirs]
  (loop [accum-steps '((0 0))
         dirs dirs]
    (if (empty? dirs)
      accum-steps
      (recur (conj accum-steps (vec-add (first accum-steps) (get dir-to-vector (first dirs)))) (rest dirs)))))

(defn max-dist [dirs]
  (let [accum-steps (accum-steps dirs)]
    (reduce max (map #(hex-dist %) accum-steps))))

;day12
(defn parse-day12-data
  ([input] (loop [accum-map {}
          in-list (map (fn [v] (map #(clojure.string/trim %) v)) (map (fn [in] (clojure.string/split in #"<->")) (clojure.string/split-lines (clojure.string/trim-newline input))))]
     (if (empty? in-list)
       accum-map
       (recur (assoc accum-map (ffirst in-list) (map #(clojure.string/trim %) (clojure.string/split (first (rest (first in-list))) #","))) (rest in-list)))))
  ([] (parse-day12-data (slurp "src/adventofcode/day12.in"))))

(defn traverse [process-map starting-node]
  (loop [visited #{}
         to-visit (list starting-node)]
    (if (empty? to-visit)
      visited
      (recur (conj visited (first to-visit)) (concat (rest to-visit) (remove visited (get process-map (first to-visit))))))))

;day 13
(defn parse-day13-data
  ([input] (map (fn [v] (list (Integer. (first v)) (Integer. (first (rest v))))) (map (fn [in] (clojure.string/split in #": ")) (clojure.string/split-lines (clojure.string/trim-newline input))))))

(defn foo
  "I don't do a whole lot."
  [x]
  (println x "Hello, World!"))
