# Clojure Advent of Code

Learning Clojure by solving the Advent Of Code problems

## License

Distributed under the Eclipse Public License either version 1.0 or (at
your option) any later version.
